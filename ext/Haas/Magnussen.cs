﻿
using System;

namespace Haas
{
    public class Magnussen
    {
        private double result;
        private double posValue = 4;
        private double negValue = -4;

        #region DO NOT TOUCH !!!!
        public void FullStop()
        {
            result = Math.Round(posValue, 1);
            Console.WriteLine("{0,4} = Math.Round({1,5}, 1)", result, posValue);
            result = Math.Round(negValue, 1);
            Console.WriteLine("{0,4} = Math.Round({1,5}, 1)", result, negValue);
            Console.WriteLine();
        }
        #endregion DO NOT TOUCH !!!!
    }
}
