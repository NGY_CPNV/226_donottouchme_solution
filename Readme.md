# Do Not Touch Me

Auteur : Nicolas Glassey

Ce projet "challenge" permet de s'entraîner à configurer les liens entre différents projets, classes.

## Comment débuter

Il vous suffit de cloner le dépôt, d'essayer de compiler...

## Résultat au début du challenge

![Start](./img/Start.PNG)

## Résultat à la fin du challenge

![Solution](./img/Solution.PNG)

## Contraintes

* Interdiction de modifier les classes (fichiers .cs)
* Interdiction de modifier les namespaces des classes

## Mot de la fin

Bonne chance ;)